if [ -z "${DISPLAY}" ] && [ "$(tty)" = "/dev/tty1" ] && [ "$(systemctl is-enabled lightdm)" != "enabled" ]; then
    exec startx
fi

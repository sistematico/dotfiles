#!/bin/bash

. $HOME/.dwm/status/icons
. $HOME/.dwm/status/colors
. $HOME/.dwm/status/functions

highlight="$blue"

clock() {
    echo -n "${highlight}${icon_clock}${nc} $(date +%H:%M)"
}

calendar() {
    local semana="$(date +%A | awk '{print substr($0,0,3)}')"
    local diames="$(date +'%d/%m')"
    echo -n "${highlight}${icon_calendar}${nc} ${semana}, ${diames}"
}

cpu() {
    local cpu=$(top -bn1 | awk '/Cpu/{print $2}')
    local cpu_usage=$(round_number $cpu)
    case $cpu_usage in
        [2-5][0-9]) echo -n "${yellow}${icon_cpu}${nc} ${cpu_usage}%" ;;
        [6-9][0-9]|100) echo -n "${red}${icon_cpu}${nc} ${cpu_usage}%" ;;
        *) echo -n "${highlight}${icon_cpu}${nc} ${cpu_usage}%" ;;
    esac
}

mem() {
    local memory="$(free -m | awk '(NR==2){$3/=1024; printf "%.1f\n",$3}')"
    local memory_integer=${memory%.*}
    case $memory in
        [5-9]) echo -n "${yellow}${icon_mem}${nc} $(printf '%3s' "$memory")GB" ;;
        1[0-6]) echo -n "${red}${icon_mem}${nc} $(printf '%3s' "$memory")GB" ;;
        *) echo -n "${highlight}${icon_mem}${nc} $(printf '%3s' "$memory")GB" ;;
    esac
}

vol() {
    local volume=$(pactl get-sink-volume @DEFAULT_SINK@ | awk -F'[^0-9]*' '{print $7;exit}')
    case $volume in
        [8-9][1-9]) echo -n "${yellow}${icon_vol}${nc} ${volume}%" ;;
        100) echo -n "${red}${icon_vol}${nc} ${volume}%" ;;
        *) echo -n "${highlight}${icon_vol}${nc} ${volume}%" ;;
    esac
}

down() {
    downloads=$(pgrep yt-dlp | wc -l)
    case $downloads in
        0) echo -n "" ;;
        [0-3]) echo -n "${green}${icon_down}${nc} $downloads" ;;
        [4-9]) echo -n "${yellow}${icon_down}${nc} $downloads" ;;
        [1-9][0-9]) echo -n "${red}${icon_down}${nc} $downloads" ;;
        *) echo -n "${highlight}${icon_down}${nc} $downloads" ;;
    esac
}

temp() {
    #local temp=$(sensors | awk '/Tctl/{print $2;exit}' | awk '{print substr($1,2); }')
    local temp=$(sensors | awk '/Tctl/{print $2;exit}' | awk '{print substr($0, 2, length($0) - 3)}')
    local temp_integer=${temp%.*}
    case $temp_integer in
        4[5-9]) echo -n "${yellow}${icon_temp}${nc} ${temp}°C" ;;
        [5-9][0-9]) echo -n "${red}${icon_temp}${nc} ${temp}°C" ;;
        *) echo -n "${highlight}${icon_temp}${nc} ${temp}°C" ;;
    esac
}

while true; do
    xprop -root -set WM_NAME "$(down) $(cpu) $(mem) $(temp) $(vol) $(calendar) $(clock)"
    sleep 5
done

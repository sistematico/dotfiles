-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- optionally enable 24-bit colour
vim.opt.termguicolors = true

-- lazy nvim
require("core.lazy")

vim.api.nvim_set_keymap("n", "<C-h>", ":NvimTreeToggle<cr>", {silent = true, noremap = true})
-- vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })

-- configs
vim.cmd("source " .. vim.fn.stdpath("config") .. "/config")
vim.cmd("source " .. vim.fn.stdpath("config") .. "/keys")

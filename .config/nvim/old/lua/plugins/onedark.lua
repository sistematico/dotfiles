return {
  "navarasu/onedark.nvim",
	style = 'cool', -- Default theme style. Choose between 'dark', 'darker', 'cool', 'deep', 'warm', 'warmer' and 'light'
	transparent = true,
	lualine = {
		transparent = true, -- lualine center bar transparency
  },
}

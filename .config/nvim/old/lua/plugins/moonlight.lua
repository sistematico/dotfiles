return {
  url = "https://codeberg.org/jthvai/moonlight.nvim.git",
  branch = "stable",
  lazy = true,
  priority = 1000,
  config = function() 
    vim.cmd([[colorscheme moonlight]])
  end
}

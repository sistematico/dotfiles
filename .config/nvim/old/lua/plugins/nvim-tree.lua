return {
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
	opts = {
		transparent = true
	},
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  config = function()
    require("nvim-tree").setup {}
  end,
}

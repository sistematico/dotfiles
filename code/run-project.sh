#!/usr/bin/env sh

# Defina suas variáveis personalizadas aqui
NAME="[SEU_PROJETO]"
DOMAIN="${NAME}.com.br"
API_DOMAIN="api.${DOMAIN}"
CONTAINER="${NAME}_postgres"
SCRIPT_PATH="$(dirname "$(realpath "$0")")"
PROJECT_PATH="$(dirname $SCRIPT_PATH)"
PROJECTS_PATH="$(dirname $PROJECT_PATH)"
POSTGRES_VERSION=14.0
DB_NAME="$NAME"
DB_USER="$NAME"
DB_PASSWORD="[SUA_SENHA]"
CONTAINER="${NAME}_postgres"

[ ! -L $PROJECTS_PATH/run.sh ] && ln -s $PROJECT_PATH/scripts/run.sh $PROJECTS_PATH/run.sh

if [ $1 ] && [ "$1" == "-a" ]; then
    \tmux a -t $NAME
    exit
fi

# Verifica se o contêiner já existe
if [ ! $1 ] || [ "$1" == "--all" ] || [ "$1" == "--podman" ]; then
    # if podman inspect "$CONTAINER" > /dev/null 2> /dev/null; then
    if podman container exists $CONTAINER; then
        echo "Contêiner $CONTAINER já existe."

        # Verifica se o contêiner está rodando
        if podman container inspect -f '{{.State.Running}}' $CONTAINER | grep false; then
            echo "Tentando iniciar o container: $CONTAINER"
            podman start $CONTAINER
        else
            echo "Contêiner $CONTAINER já está em execução."
        fi
    else
        echo "Criando e iniciando contêiner $CONTAINER."

        # Puxe a imagem do PostgreSQL 14.0
        podman pull postgres:$POSTGRES_VERSION

        # Execute o contêiner com as variáveis de ambiente personalizadas
        podman run -d \
        --name $CONTAINER \
        -e POSTGRES_DB=$DB_NAME \
        -e POSTGRES_USER=$DB_USER \
        -e POSTGRES_PASSWORD=$DB_PASSWORD \
        -p 5432:5432 \
        postgres:$POSTGRES_VERSION
    fi
fi

if [ ! $1 ] || [ "$1" == "--all" ] || [ "$1" == "--tmux" ]; then
    \tmux -f ~/.tmux-code.conf new-session -d -A -s $NAME -n home
    \tmux send-keys -t $NAME:home "clear ; cd $PROJECTS_PATH/$DOMAIN" ENTER

    \tmux new-window -t $NAME -n client -d
    \tmux send-keys -t $NAME:client "clear ; cd $PROJECTS_PATH/$DOMAIN ; bun run dev" ENTER

    \tmux new-window -t $NAME -n server -d
    \tmux send-keys -t $NAME:server "clear ; cd $PROJECTS_PATH/$API_DOMAIN ; node --watch-path=./src src/index.js" ENTER

    #\tmux new-window -t $NAME -n edit -d
    #\tmux send-keys -t $NAME:edit "clear ; cd $PROJECTS_PATH ; vim -p run.sh ~/.tmux-code.conf" ENTER

    \tmux select-window -t 1
fi

if [ ! $1 ] || [ "$1" == "--all" ] || [ "$1" == "--code" ]; then
    firefox 'http://localhost:5173' &
    code $PROJECTS_PATH/api.${DOMAIN}
    code $PROJECTS_PATH/${DOMAIN}
fi

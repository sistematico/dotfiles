#!/usr/bin/zsh

if ! (( $+commands[identify] )) || [ ! $1 ] 
then
  exit
fi

function identify-image() {
  image=$(identify "$1" 2> /dev/null | awk '{print $2}')
  
  if [ -z "$image" ]
  then
    exit
  else
    echo $image
  fi
}

function from-webp() {
  dwebp "$1" -o "${2}.png"
}

function to-webp() {
  cwebp -q 75 "$1" -o "${2}.webp"
}

echo $@

for file in $@
do 
  original="$file"
  fullpath=$original:h
  ext=$original:t:e
  filename=$original:t:r

  if [[ "$(identify-image $file)" == "WEBP" ]]
  then
    from-webp "$file" "$fullpath/$filename"
  else
    to-webp "$file" "$fullpath/$filename"
  fi
done


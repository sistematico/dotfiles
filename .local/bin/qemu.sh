#!/bin/bash

function runvm() {
  qemu-system-x86_64 -hda $VM_IMAGE $QEMU_OPTIONS $SSH_MODE $HOST_SHARE $@
}

case $1 in
	rocky)
    ISO_IMAGE="$HOME/iso/rocky/Rocky-9.2-x86_64-minimal.iso"
    VM_IMAGE="$HOME/.qemu/vms/rocky.qcow2"
  ;; 
	*)
    ISO_IMAGE="$HOME/iso/rocky/Rocky-9.2-x86_64-minimal.iso"
    VM_IMAGE="$HOME/.qemu/vms/rocky.qcow2"
  ;; 
esac

VM_PATH=$(dirname "$VM_IMAGE")
[ ! -d $VM_PATH ] && \mkdir -p $VM_PATH

# Opções de configuração do QEMU (ajuste conforme necessário)
QEMU_OPTIONS="-m 2048 -smp 2 -cpu host -enable-kvm -nographic"
SSH_MODE="-net user,hostfwd=tcp::2222-:22 -net nic" # ssh -p 2222 usuário@localhost
VNC_MODE="-vnc :0"
HOST_SHARE="-fsdev local,id=fsdev0,path=$HOME/publico,security_model=mapped -device virtio-9p-pci,fsdev=fsdev0,mount_tag=publico"

# Verificar se a imagem da VM existe
if [ -f "$VM_IMAGE" ]; then
	echo "Iniciando a máquina virtual existente..."
	runvm
else
	echo "Criando uma nova máquina virtual..."
	qemu-img create -f qcow2 $VM_IMAGE 20G
	runvm -cdrom $ISO_IMAGE
fi

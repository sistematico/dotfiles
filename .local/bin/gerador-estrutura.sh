#!/bin/bash

# Função para criar um diretório e seus subdiretórios recursivamente
criarDiretorio() {
    if [ ! -d "$1" ]; then
        mkdir -p "$1"
        echo "Diretório criado: $1"
    fi
}

# Função para criar a estrutura de diretórios para o padrão Clean Code
criarEstruturaCleanCode() {
    criarDiretorio "src"
    criarDiretorio "src/controllers"
    criarDiretorio "src/models"
    criarDiretorio "src/routes"
    echo "Estrutura Clean Code criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão DDD
criarEstruturaDDD() {
    criarDiretorio "src"
    criarDiretorio "src/application"
    criarDiretorio "src/domain"
    criarDiretorio "src/infra"
    criarDiretorio "src/interfaces"
    echo "Estrutura DDD criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão TDD
criarEstruturaTDD() {
    criarDiretorio "src"
    criarDiretorio "src/tests"
    echo "Estrutura TDD criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão MVC
criarEstruturaMVC() {
    criarDiretorio "src"
    criarDiretorio "src/controllers"
    criarDiretorio "src/models"
    criarDiretorio "src/views"
    echo "Estrutura MVC criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão MVVM
criarEstruturaMVVM() {
    criarDiretorio "src"
    criarDiretorio "src/models"
    criarDiretorio "src/views"
    criarDiretorio "src/viewmodels"
    echo "Estrutura MVVM criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão Flux
criarEstruturaFlux() {
    criarDiretorio "src"
    criarDiretorio "src/actions"
    criarDiretorio "src/components"
    criarDiretorio "src/stores"
    criarDiretorio "src/utils"
    echo "Estrutura Flux criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão Hexagonal
criarEstruturaHexagonal() {
    criarDiretorio "src"
    criarDiretorio "src/application"
    criarDiretorio "src/domain"
    criarDiretorio "src/infrastructure"
    echo "Estrutura Hexagonal criada com sucesso."
}

# Função para criar a estrutura de diretórios para o padrão Event Sourcing/CQRS
criarEstruturaEventSourcingCQRS() {
    criarDiretorio "src"
    criarDiretorio "src/commands"
    criarDiretorio "src/queries"
    criarDiretorio "src/events"
    criarDiretorio "src/projections"
    echo "Estrutura Event Sourcing/CQRS criada com sucesso."
}

# Verifica qual estrutura de diretórios deve ser criada com base nos argumentos passados na linha de comando
if [ "$#" -ne 1 ]; then
    echo "Uso: $0 <padrão-arquitetura>"
    exit 1
fi

padraoArquitetura=$1

case "$padraoArquitetura" in
    "clean-code")
        criarEstruturaCleanCode
        ;;
    "ddd")
        criarEstruturaDDD
        ;;
    "tdd")
        criarEstruturaTDD
        ;;
    "mvc")
        criarEstruturaMVC
        ;;
    "mvvm")
        criarEstruturaMVVM
        ;;
    "flux")
        criarEstruturaFlux
        ;;
    "hexagonal")
        criarEstruturaHexagonal
        ;;
    "event-sourcing-cqrs")
        criarEstruturaEventSourcingCQRS
        ;;
    *)
        echo "Padrão de arquitetura inválido."
        exit 1
        ;;
esac

exit 0


#!/usr/bin/env bash
#
# Arquivo: gnome-setup.sh
#
# Um script em shell usando o dialog para configurar o GNOME
#
# Feito por Lucas Saliés Brum a.k.a. sistematico, <sistematico@gmail.com>
#
# Criado em: 04/10/2023 11:08:37
# Última alteração: 04/10/2023 11:08:41

DIALOG=$(which dialog --clear --keep-tite --title 'GNOME' --backtitle 'GNOME Config')
export DIALOGRC=$HOME/.dialog/vermelho.cfg

function botoes() {
  botoes=$(
    $DIALOG --stdout \
      --checklist 'Selecione os botões das janelas' \
      0 0 0 \
      'minimize,' 'Minimizar' on \
      'maximize,' 'Maximizar' on \
      'close,' 'Fechar' on \
      'nenhum' 'Fechar' off
  )

  [ $? -ne 0 ] || [ -z "$botoes" ] && return



  if [[ "$botoes" == *"nenhum"* ]]; then
    gsettings set org.gnome.desktop.wm.preferences button-layout ":$buttons"
  else
    buttons=$(echo $botoes | sed 's/,$//')
    buttons=":minimize,maximize,close"
    notify-send "$buttons"
    gsettings set org.gnome.desktop.wm.preferences button-layout ':'${buttons}
  fi
}

function center_new_windows() {
  $DIALOG --yesno 'Centralizar novas janelas?' 0 0

  if [ $? = 0 ]; then
    gsettings reset org.gnome.mutter center-new-windows true
  else
    gsettings reset org.gnome.mutter center-new-windows false
  fi
}

function drag_n_drop() {
  $DIALOG --yesno 'Abrir pastas ao arrastar?' 0 0

  if [ $? = 0 ]; then
    gsettings set org.gnome.nautilus.preferences open-folder-on-dnd-hover true
  else
    gsettings set org.gnome.nautilus.preferences open-folder-on-dnd-hover false
  fi
}

function numlock() {
  numlock=$(
    $DIALOG --stdout                                          \
      --menu 'Escolha o estado da tecla NumLock ao iniciar:'  \
      0 0 0                                                   \
      ligado 'Ligado'                                         \
      desligado 'Desligado'                                   \
      ultimo 'Último estado'
  )

  case $numlock in
    "ligado") 
      gsettings set org.gnome.desktop.peripherals.keyboard numlock-state true
    ;;
    "desligado")
      gsettings set org.gnome.desktop.peripherals.keyboard numlock-state false    
    ;;
    "ultimo")
      gsettings set org.gnome.desktop.peripherals.keyboard numlock-state true    
    ;;
    *) 
      return
    ;;
  esac

  [ $? -ne 0 ] && return
}

while :; do
  resposta=$(
    $DIALOG --stdout    \
      --menu 'Ações:'   \
      0 0 0             \
      1 'Botões'        \
      2 'Centralizar'   \
      3 'Drag N Drop'   \
      4 'NumLock'       \
      5 'Perder tempo'  \
      0 'Sair'
  )

  [ $? -ne 0 ] && break

  case "$resposta" in
  1) botoes ;;
  2) center_new_windows ;;
  3) drag_n_drop ;;
  4) numlock ;;
  0) break ;;
  esac
done

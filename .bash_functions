# vars
STORAGE="$HOME"

# Base
list-fonts() {
    [ ! $1 ] && return
    fc-list | awk -F':' '{print $2}' | grep -i $1 | awk '{$1=$1};1' | sort | uniq
}

# Void Linux
# create-package() {
#   OLDPWD=$(pwd)

#   # git clone --depth=1 https://github.com/void-linux/void-packages
#   cd ~/src/void-packages
#   # ./xbps-src binary-bootstrap
#   # echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
#   # curl -sL 'https://gist.githubusercontent.com/sistematico/1704133a6276429d168319fbdd4559ef/raw/4bbb37f5eba269ebde4b5208a954809fbaad0a47/template' > ./srcpkgs/$1/template
#   vim ./srcpkgs/$1/template
#   ./xbps-src pkg $1
#   sudo xbps-install --repository=hostdir/binpkgs $1

#   cd $OLDPWD
# }

# rsync
fullsync() {
    [ ! -d $STORAGE/vps/$1 ] && mkdir -p $STORAGE/vps/${1}
    
    rsync -aAXvzz \
    --exclude-from "$HOME/.config/rsync-excludes.list" \
    root@${1}:/ $STORAGE/vps/${1}/
}

# git
commit() {
  [ -f .commit ] && msg="$(cat .commit)" || msg="Commit automático"
  [ -f .commit ] && msg="$(cat .commit)" || msg="Alterações: $(git status -s -z)"
  [ $1 ] && msg="$@"
  git add .
  git commit -m "$msg"
  git push
}

autocommit() {
    if [ -d .git ]; then
        curl -s -L https://git.io/JzKB2 -o .git/hooks/post-commit
        chmod +x .git/hooks/post-commit
        git config --local commit.template .commit

        if [ ! -f .commit ] || [ ! -s .commit ]; then
            echo "Update automático" > .commit
        fi

        if [ ! -f .gitignore ] || [ ! -s .gitignore ]; then
            echo ".commit" > .gitignore
        else
            if ! grep -Fxq ".commit" .gitignore 2> /dev/null; then
                echo ".commit" >> .gitignore        
            fi
        fi
    fi
}

rename-branch() {
  git branch -m $1 $2
  git push origin :${1}
  git push origin -u $2
}

new-branch() {
  git checkout -b --orphan $1
  git rm --cached -r .
  #git rm -rf .
}

delete-branch() {
  old=$(git rev-parse --abbrev-ref HEAD)
  git checkout -
  git branch -d $old
  git push origin --delete $old
}

create-patch() {
  #cd program-directory
  #git add filechanges...
  #git commit (write a clear patch description)
  git format-patch --stdout HEAD^ > $(basename $(pwd))-$(git log -1 --pretty=format:%B | awk '{print tolower($1)}')-$(date '+%Y%m%d')-$(git rev-parse --short HEAD).diff
}

# gtk
# gnome
chshell() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        echo
        for tema in $(/usr/bin/ls /usr/share/themes); do
            if [ -d /usr/share/themes/${tema} ]; then
                if [ -d /usr/share/themes/${tema}/gnome-shell ]; then
                    echo $tema
                fi
            fi
        done
        for tema in $(/usr/bin/ls $HOME/.local/share/themes); do
            if [ -d $HOME/.local/share/themes/${tema} ]; then
                if [ -d $HOME/.local/share/themes/${tema}/gnome-shell ]; then
                    echo $tema
                fi
            fi
        done
        return
    fi

    if [ ! -d $HOME/.local/share/themes/$1/gnome-shell ]; then
        if [ ! -d /usr/share/themes/$1/gnome-shell ]; then
            echo "Tema inválido"
            return
        fi
    fi
    echo "Trocando o tema"
    gsettings set org.gnome.shell.extensions.user-theme name "$1"
}

chgtk() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        for tema in $(/usr/bin/ls /usr/share/themes); do
            if [ -d /usr/share/themes/${tema} ]; then
                if [ -d /usr/share/themes/${tema}/gtk-3.0 ]; then
                    echo $tema
                fi
            fi
        done
        if [ -d $HOME/.local/share/themes ]; then 
            for tema in $(/usr/bin/ls $HOME/.local/share/themes); do
                if [ -d $HOME/.local/share/themes/${tema} ]; then
                    if [ -d $HOME/.local/share/themes/${tema}/gtk-3.0 ]; then
                        echo $tema
                    fi
                fi
            done
        fi
        return
    fi

    if [ ! -d $HOME/.local/share/themes/$1/gtk-3.0 ]; then
        if [ ! -d /usr/share/themes/$1/gtk-3.0 ]; then
            echo "Tema inválido"
            return
        fi
    fi
    echo "Trocando o tema"    
    gsettings set org.gnome.desktop.interface gtk-theme "$1"
}

chicon() {
    if [ ! $1 ]; then
        echo "Temas disponíveis:"
        for tema in $(/usr/bin/ls /usr/share/icons); do
            if [ -d "/usr/share/icons/${tema}" ]; then
                echo $tema
            fi            
        done
        for tema in $(/usr/bin/ls $HOME/.local/share/icons); do
            if [ -d "$HOME/.local/share/icons/${tema}" ]; then
                echo $tema
            fi
        done
        return
    fi

    if [ ! -d $HOME/.local/share/icons/$1 ]; then
        if [ ! -d /usr/share/icons/$1 ]; then
            echo "Tema inválido"
            return
        fi
    fi
    echo "Trocando o tema" 
    gsettings set org.gnome.desktop.interface icon-theme "$1"
}

rsticon() {
    gsettings reset org.gnome.desktop.interface icon-theme
}

rstshell() {
    gsettings reset org.gnome.shell.extensions.user-theme name
}

rstgtk() {
    gsettings reset org.gnome.desktop.interface gtk-theme
}

rstcursor() {
    gsettings reset org.gnome.desktop.interface cursor-theme
}

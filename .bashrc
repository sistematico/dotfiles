#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto --group-directories-first'
alias grep='grep --color=auto'

PS1='[\u@\h \W]\$ '

[ -f $HOME/.bash_aliases ] && . $HOME/.bash_aliases
[ -f $HOME/.bash_functions ] && . $HOME/.bash_functions
[ -f /usr/share/doc/pkgfile/command-not-found.bash ] && . /usr/share/doc/pkgfile/command-not-found.bash

# Git
commit() {
  [ -f .commit ] && msg="$(cat .commit)" || msg="Commit automático"
  [ -f .commit ] && msg="$(cat .commit)" || msg="Alterações: $(git status -s -z)"
  [ $1 ] && msg="$@"
  git add .
  git commit -m "$msg"
  git push
}

eval "$(starship init bash)"

